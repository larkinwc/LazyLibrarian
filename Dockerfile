FROM python:3.6

EXPOSE 5299


RUN mkdir /code

# Copy code
WORKDIR /code
COPY . /code

CMD python LazyLibrarian.py -d && tail -f /dev/null